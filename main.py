import argparse
from selenium import webdriver
from properties_page import PropertiesPage


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--pause_on', help='Pause and wait for user key press after every test.',
                        action='store_true')
    args = parser.parse_args()
    results = []

    driver = webdriver.Chrome()
    prop = PropertiesPage(driver)
    results.append(prop.sort_properties('price', 'desc', args.pause_on))

    driver = webdriver.Chrome()
    prop = PropertiesPage(driver)
    results.append(prop.sort_properties('price', 'asc', args.pause_on))

    driver = webdriver.Chrome()
    prop = PropertiesPage(driver)
    results.append(prop.sort_properties('title', 'asc', args.pause_on))

    driver = webdriver.Chrome()
    prop = PropertiesPage(driver)
    results.append(prop.sort_properties('title', 'desc', args.pause_on))

    driver = webdriver.Chrome()
    prop = PropertiesPage(driver)
    results.append(prop.check_images_width(args.pause_on))

    print(f'{results.count(True)} out of {len(results)} tests passed!')
