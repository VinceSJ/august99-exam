# August99 Exam

QA engineer exam answers for my August 99 application.


1. Requirements:
    1. [Python](https://www.python.org/downloads/) (at least version 3.4)
    2. [Selenium](https://pypi.org/project/selenium/) (at least version 3.141.0)
    3. [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads) (at least version 84)

2. Execution:
    1.  Download the two Python files
        1. main.py
        2. properties_page.py
    2. Navigate to the location of main.py
    3. Run using the command `python main.py`
    4. For viewing convenience, the option `--pause_on` is added which pauses and waits for a user key press after every test.  
        - Usage: `python main.py --pause_on`