
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from time import sleep


class PropertiesPage:
    URL = 'https://ap-empire.agentimage.com/properties'
    SORT_DROPDOWN_XPATH = "//*[@class='sort-sel aios-listings-sorter']"
    SORT_OPTIONS = {'price': {'asc': 'Price Ascending', 'desc': 'Price Descending'},
                    'title': {'asc': 'A-Z', 'desc': 'Z-A'}}
    PRICE_CLASS = 'list-price'
    TITLE_CLASS = 'prop-title'
    PROP_IMG_XPATH = "//img[@class='main-img wp-post-image']"
    PROP_IMG_WIDTH = 300

    def __init__(self, driver):
        self.driver = driver

    def sort_properties(self, option, order, pause=False, delay=5):
        """This function opens the properties page and interacts with the sorting dropdown
            Parameters:
            1. option = select 'price' or 'title' to sort accordingly
            2. order = select 'asc' or 'desc' to sort accordingly
            3. pause = If True, pause and wait for user key press after every test
            4. delay = Sets time to wait in seconds"""
        propList = []
        class_identifier = ''
        result = False

        if option == 'price':
            class_identifier = self.PRICE_CLASS
        elif option == 'title':
            class_identifier = self.TITLE_CLASS
        else:
            raise ValueError("The variable [option] must be either 'price' or 'title'")
        
        try:    
            self.driver.get(self.URL)
            WebDriverWait(self.driver, delay).until(EC.presence_of_element_located(
                (By.XPATH, self.SORT_DROPDOWN_XPATH)))
            sleep(delay)
            dropdown = Select(self.driver.find_element_by_xpath(self.SORT_DROPDOWN_XPATH))
            print(f'Sorting properties by {self.SORT_OPTIONS[option][order]}')
            dropdown.select_by_visible_text(self.SORT_OPTIONS[option][order])
            url_pattern = f'sort={option}&order={order}'
            WebDriverWait(self.driver, delay).until(EC.url_contains(url_pattern))

            for element in self.driver.find_elements_by_class_name(class_identifier):
                if element.text != '':
                    if option == 'price':
                        propList.append(int(
                            ''.join([char for char in element.text if char.isdigit()])))
                    else:
                        propList.append(element.text)
            if order == 'asc':
                sortedpropList = sorted(propList)
            elif order == 'desc':
                sortedpropList = sorted(propList)[::-1]
            else:
                raise ValueError("The variable [order] must be either 'asc' or 'desc'")

            if propList == sortedpropList:
                print('[PASS] The order is correct!')
                result = True
            else:
                print(f'[FAIL] The order is incorrect!')
                print(f'Current Order : {propList}\nExpected Order: {sortedpropList}')
        except TimeoutError:
            print('Page took too long to load!')
        finally:
            if pause:
                input('Press any key to stop viewing current scenario...')
            self.driver.close()
            return result


    def check_images_width(self, pause=False, delay=10):
        """This function opens the properties page and validates whether the displayed images are
            equal to 'PROP_IMG_WIDTH'
            Parameters:
            1. pause = If True, pause and wait for user key press after every test
            2. delay = Sets time to wait in seconds"""
        result = False
        try:    
            self.driver.get(self.URL)
            WebDriverWait(self.driver, delay).until(
                EC.presence_of_element_located((By.XPATH, self.PROP_IMG_XPATH)))
            imageList = self.driver.find_elements_by_xpath(self.PROP_IMG_XPATH)
            counter = 0
            if imageList != []:    
                for image in imageList:
                    if image.size['width'] == 0:
                        continue
                    if int(image.size['width']) != self.PROP_IMG_WIDTH:
                        counter+=1
                if counter != 0:
                    print(f'[FAIL] {counter} images do not have a width of {self.PROP_IMG_WIDTH}px')
                else:
                    print(f'All property images have {self.PROP_IMG_WIDTH}px width')
                    result = True
            else:
                print('No images were detected! Consider updating the XPATH')
        except TimeoutError:
            print('Page took too long to load!')
        finally:
            if pause:
                input('Press any key to stop viewing current scenario...')
            self.driver.close()
            return result
